# Le-Fil électronique

## Présentation
L'objectif du projet est de réaliser un système informatique permettant la gestion d'un journal étudiant en ligne. Le projet permettra la création d'articles, leur catégorisation, leur recherche, la gestion du suivi éditorial, et l'apposition de commentaires.

Le cahier des charges est accessible à l'adresse https://librecours.net/dawa/projets-2020-II/co/le-fil.html


## Historisation
Pour pouvoir garder une trace de tout ce qu'il se passe dans le système, les commentaires articles ne seront jamais supprimés de la BDD mais on passera la valeur de leur clé "supprime" à true. De plus, Une classe Action permettra de garder l'historique des actions faites sur ces derniers ainsi que leurs auteurs.


## Objets et Propriétés
* Publication (tout est NOT NULL, la casse est abstraite)
    * titre: string
    * auteur: string
    * date: date
    * visible: bool (pour un article, il s'agit de si il est publié ou non, pour un commentaire, il s'agit de si il est visible ou masqué)
    * suprimme: bool
    * emphase: bool (pour un article, il s'agit de si il fait partie de "à l'honneur" ou non, pour un commentaire, il s'agit de si il est en exergue ou non)
* Commentaire (hérite de Publication)
    * contenu: string NOT NULL
* Article (hérite de Publication)
    * statut: Statut NOT NULL
    * commentaire: string
* Bloc (tout est NOT NULL)
    * titre: string
    * image: bool (si true, il s'agit d'une image : le contenu est alors le chemin de l'image)
    * contenu: string
* Rubrique
    * nom: string NOT NULL
    * parent: Rubrique
* Acteur (classe abstraite)
    * nom: string NOT NULL
* Action (tout est NOT NULL)
    * acteur: Acteur
    * publication: Publication
    * action: string
* MotCle
    * article: Article NOT NULL
    * mot: string NOT NULL

## Acteurs
* Administrateur
    * Peut tout faire
* Auteur
    * Peut créer un article
    * Voir ses articles en cours de rédaction, supprimer un article, récupérer un article supprimé
    * Peut modifier l'état d'un de ses articles en "soumis"
* Éditeur
    * Peut voir l'ensemble des articles, par auteur, par date, par statut (voir ci-après)
    * Peut corriger les articles
    * Peu modifier le statut des articles et la valeur de "commentaire", de "visible" et de "emphase"
    * Peut insérer, supprimer et modifier dans MotCle
    * Peut insérer, supprimer et modifier dans Rubrique
    * Peut associer les articles à une ou plusieurs rubriques
    * Peut associer un article à un autre
* Lecteur
    * Peut selectionner dans Article
    * Peut accèder aux articles par rubrique
    * Peut accèder aux articles par mots clés
    * Peut accèder aux articles "à l'honneur"
    * Peut accéder aux articles liés depuis un article lu
    * Peut insérer dans Commentaire
    * Peut supprimer dans Commentaire (si il en est l'auteur)
    * Peut selectionner dans commentaires (non masqués)
* Modérateur
    * Peut modifier la valeur de "masque", "supprime" et de "emphase" d'un commentaire